# AutomakeParser

[![Build Status](https://travis-ci.org/demosdemon/AutomakeParser.png?branch=master)](https://travis-ci.org/demosdemon/AutomakeParser)
[![Code Climate](https://codeclimate.com/github/demosdemon/AutomakeParser.png)](https://codeclimate.com/github/demosdemon/AutomakeParser)
[![Dependency Status](https://gemnasium.com/demosdemon/AutomakeParser.png)](https://gemnasium.com/demosdemon/AutomakeParser)
[![Coverage Status](https://coveralls.io/repos/demosdemon/AutomakeParser/badge.png)](https://coveralls.io/r/demosdemon/AutomakeParser)

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'AutomakeParser'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install AutomakeParser

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
