require 'AutomakeParser/version'
require 'AutomakeParser/automake_file'

## require treetop and polyglot in case automake_helper.rb isn't built
require 'treetop'
require 'polyglot'
require 'AutomakeParser/automake_helper'

module AutomakeParser
  # Your code goes here...
end
