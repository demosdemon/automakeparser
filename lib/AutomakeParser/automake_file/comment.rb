require 'treetop'

module AutomakeParser
  module AutomakeFile
    class Comment < Treetop::Runtime::SyntaxNode

      def resolve
        res = {}
        res[:comment] = text_value.strip.gsub(/^\s*#/, "#") if !empty?
        res


      end
    end
  end
end
