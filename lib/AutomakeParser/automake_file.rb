require 'logger'

SUBDIR_GLOB = File.join(File.dirname(__FILE__), File.basename(__FILE__, '.*'), '*.rb')
Dir.glob(SUBDIR_GLOB).each { |f| require f }

logger = Logger.new STDOUT
logger.level = Logger::DEBUG if $DEBUG
logger.level = Logger::INFO if not $DEBUG

module AutomakeParser
  module AutomakeFile

    def self.conditional(node)
      res = { conditional: node.cond.text_value.to_sym }
      res.merge! node.target.resolve if !node.target.text_value.empty?
      res.merge! node.c.resolve if !node.c.text_value.empty?
      res
    end


  end
end
