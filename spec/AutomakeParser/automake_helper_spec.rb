require 'spec_helper'
include AutomakeParser

describe AutomakeHelper do
  let(:parser) { AutomakeHelperParser.new }

  describe 'comments' do
    let(:valid_comment) { '# this is a comment' }
    let(:multiple_comments) { ([valid_comment] * 10) }
    let(:whitespace) { ' ' * 10 }

    describe "parse a valid comment line with EOF instead of NL" do
      before { @res = parser.parse(valid_comment) }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 1 }
      it { @res.resolve.first.should == { comment: valid_comment } }
    end

    describe "parse a valid comment line with a NL" do
      before { @res = parser.parse(valid_comment + "\n") }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 1 }
      it { @res.resolve.first.should == { comment: valid_comment } }
    end

    describe "parse multi-line comments into one block" do
      let(:comments) { multiple_comments.join("\n") }
      before { @res = parser.parse(comments) }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 1 }
      it { @res.resolve.first.should == { comment: comments } }
    end

    describe "parse comments with leading whitespace" do
      before { @res = parser.parse(whitespace + valid_comment) }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 1 }
      it { @res.resolve.first.should == { comment: valid_comment } }
    end

    describe "parse multi-line comments with leading whitespace into one block" do
      let(:white_comments) { multiple_comments.map { |c| whitespace + c }.join("\n") }
      let(:comments) { multiple_comments.join("\n") }
      before { @res = parser.parse(white_comments) }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 1 }
      it { @res.resolve.first.should == { comment: comments } }
    end

  end

  describe 'blank lines' do
    describe "empty input" do
      before { @res = parser.parse("") }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 0 }
    end

    describe "one NL character" do
      before { @res = parser.parse("\n") }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 0 }
    end

    describe "many NL characters" do
      before { @res = parser.parse("\n" * 10) }

      it { @res.should_not be_nil }
      it { @res.resolve.length.should == 0 }
    end
  end

  describe 'if expressions' do

  end

  describe 'else expressions' do

  end

  describe 'endif expressions' do

  end

  describe 'rule expressions' do

  end

  describe 'assignment expressions' do

  end

  describe 'include expressions' do

  end
end
