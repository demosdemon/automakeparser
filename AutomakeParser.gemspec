# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'AutomakeParser/version'

Gem::Specification.new do |spec|
  spec.name          = 'AutomakeParser'
  spec.version       = AutomakeParser::VERSION
  spec.authors       = ['Joachim LeBlanc']
  spec.email         = ['demosdemon@gmail.com']
  spec.description   = 'Parse an automake project creating a CocoaPods podspec for that project'
  spec.summary       = 'Parse an automake project creating a CocoaPods podspec for that project'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'treetop'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'coveralls'
end
